﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CameraController : MonoBehaviour {

	public Text countText;
	public Text itemText;
	public Text speedText;
	public Text gameOverText;
	public ObstacleGenerator obs;

	public GameObject restartBtn;
	public GameObject backToMenuBtn;

	public bool gameOver;

	private const int scoreBonusTextTime = 10;
	private const int scoreBonus = 100;
	private const int itemFrameNum = 600;

	private int boosterCounter;
	public int itemCounter; 

    private const float angleSpeed = 2.0f;
	private int score; 


	// Use this for initialization
	void Start () {
		score = 0;

		boosterCounter = 0; 
		itemCounter = 0;

		gameOver = false;

		setScoreText ();
		setItemText ();
		setSpeedText ();

		restartBtn.SetActive (false);
		backToMenuBtn.SetActive (false);

	}
	
	// Update is called once per frame
	void Update () {
		
		if (!gameOver) {

			score += 2;

			this.transform.RotateAround(Vector3.zero, Vector3.forward, -Input.acceleration.x * angleSpeed);

			// need to be commented out!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			// if (Input.GetKey (KeyCode.LeftArrow)) {
			// 	this.transform.RotateAround (Vector3.zero, Vector3.forward, angleSpeed);
			// }
			// if (Input.GetKey (KeyCode.RightArrow)) {
			// 	this.transform.RotateAround (Vector3.zero, Vector3.forward, -angleSpeed);
			// }
		}

		if (boosterCounter != 0) {
			boosterCounter--;
		}
		if (itemCounter != 0) {
			itemCounter--;
		}

			
		setScoreText ();
		setItemText ();
		setSpeedText ();

	}

	void OnTriggerEnter(Collider other) {
		
		if (other.gameObject.CompareTag ("Item")) {
			Debug.Log ("item");
			if (!gameOver)
				itemCounter = itemFrameNum;

		} else if (other.gameObject.CompareTag ("Obstacle") || other.gameObject.CompareTag("FakeObstacle")) {
			Debug.Log ("obstacle");
			gameOverText.text = "Game Over!\nYour score: " + score.ToString ();
			gameOver = true;
			restartBtn.SetActive (true);
			backToMenuBtn.SetActive (true);

		} else if (other.gameObject.CompareTag ("Booster")) {
			Debug.Log ("booster");
			if (!gameOver) {
				countText.text = "Score: " + score.ToString () + "+ 100";
				score += scoreBonus;
				boosterCounter = scoreBonusTextTime;
			}
		}
	}

	void setScoreText() {
		if (boosterCounter > 0) {
			countText.text = "Score: " + score.ToString () + "+ 100";
		} else {
			countText.text = "Score: " + score.ToString ();
		}
	}

	void setItemText() {
		if (!gameOver) {
			if (itemCounter > 0) {
				Debug.Log (itemFrameNum);
				itemText.text = "Item remaining time: " + (itemCounter / 60 + 1).ToString ();
			} else {
				itemText.text = "";
			}
		}
	}

	void setSpeedText() {
		double speed = System.Math.Round (obs.getCurrSpeed (), 2);
		speedText.text = "Speed: " + speed.ToString ();
	}
}
