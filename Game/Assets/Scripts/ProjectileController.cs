﻿using UnityEngine;
using System.Collections;

public class ProjectileController : MonoBehaviour {

	public Vector3 velocity;

	public AudioClip destroySound;

	private AudioSource source;

	private bool canDestroy;

	// Use this for initialization
	void Awake () {
		source = GetComponent<AudioSource> ();
	}

	void Start() {
		canDestroy = false;
	}

	// Update is called once per frame
	void Update () {
		this.transform.Translate (velocity * Time.deltaTime);
		if (canDestroy && !source.isPlaying) {
			Destroy (this.gameObject);
		}
	}

    void OnTriggerEnter(Collider other)
    {

        if (!canDestroy && other.gameObject.CompareTag("Obstacle"))
        {
            //Debug.Log("obstacle");
			source.PlayOneShot(destroySound, 1f);
			//while (source.isPlaying);
			Destroy (other.gameObject);
			canDestroy = true;
        }
       
    }
}
