﻿using UnityEngine;
using System.Collections;

public class LightMovement : MonoBehaviour {

	public Camera player; 

	private const float angleSpeed = 3; 
	private const float radius = 10; 
	private const float radiusSpeed = 2;


	// Update is called once per frame
	void Update () {
		Vector3 playerPos = player.transform.position;

		this.transform.RotateAround (playerPos, Vector3.forward, angleSpeed);
		Vector3 goToPos = (this.transform.position - playerPos).normalized * radius;
		this.transform.position = Vector3.MoveTowards (this.transform.position, goToPos, Time.deltaTime * radiusSpeed);

	}
}
