﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SettingsBtnListener : MonoBehaviour {

	public Button btn;

	void Start() {
		Button b = btn.GetComponent<Button> (); 
		b.onClick.AddListener (goToSettings);

	}

	public void goToSettings() {
		//Debug.Log ("S");
		SceneManager.LoadScene ("SettingsScene");
	}
}
