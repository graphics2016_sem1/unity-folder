﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class StartGameButtonListener : MonoBehaviour {

	public Button btn;
	public GameObject l;

	private bool afterClick;

	private const float angleSpeed = 1.5f;

	void Start() {
		Button b = btn.GetComponent<Button> (); 
		b.onClick.AddListener (setAfterClick);

		afterClick = false;
	}

	void Update() {
		if (!afterClick) {
			l.transform.RotateAround (Vector3.zero, Vector3.forward, angleSpeed);
		} else {
			if (l.transform.position.z < 100) {
				l.transform.RotateAround (Vector3.zero, Vector3.forward, angleSpeed);
				l.transform.position = new Vector3 (l.transform.position.x, l.transform.position.y, l.transform.position.z + 1);
			} else {
				GoToGame ();
			}
		}
	}

	public void setAfterClick() {

		afterClick = true;
	}
		
	public void GoToGame() {
		SceneManager.LoadScene ("GameScene");
	}
}
