﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class BackToMenuBtnListener : MonoBehaviour {

	public Button btn;

	void Start() {
		Button b = btn.GetComponent<Button> (); 
		b.onClick.AddListener (BackToMenu);

	}

	public void BackToMenu() {
		//Debug.Log ("S");
		SceneManager.LoadScene ("MenuScene");
	}
}
