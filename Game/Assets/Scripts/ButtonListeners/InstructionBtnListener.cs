﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class InstructionBtnListener : MonoBehaviour {

	public Button btn;

	void Start() {
		Button b = btn.GetComponent<Button> (); 
		b.onClick.AddListener (GoToInstruction);

	}

	public void GoToInstruction () {
		//Debug.Log ("S");
		SceneManager.LoadScene ("InstructionScene");
	}
}
