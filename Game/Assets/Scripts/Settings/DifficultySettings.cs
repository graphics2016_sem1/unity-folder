﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DifficultySettings : MonoBehaviour {

	public Slider difficultySlider; 
	public Text speedDisplay;

	// Use this for initialization
	void Start () {
		difficultySlider.onValueChanged.AddListener (delegate {setSpeed ();});
	}
	
	public void setSpeed() {
		float s = (float)System.Math.Round (difficultySlider.value, 2);
		speedDisplay.text = s.ToString ();
	}
}
