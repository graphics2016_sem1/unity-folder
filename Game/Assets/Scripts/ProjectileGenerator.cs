﻿using UnityEngine;
using System.Collections;

public class ProjectileGenerator : MonoBehaviour {

	public CameraController player; 
	public GameObject projectileTemplate;

	public AudioClip shootSound;

	private const float speed = 20;

	private AudioSource source;

	void Awake() {

		source = GetComponent<AudioSource> ();
	}

	// Update is called once per frame
	void Update () {
		if (player.itemCounter > 0 && !player.gameOver)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);
                if (touch.phase == TouchPhase.Ended)
                {
					source.PlayOneShot (shootSound, 1f);

                    GameObject projectile = GameObject.Instantiate<GameObject>(projectileTemplate);
                    projectile.transform.position = player.transform.position;

                    //Vector3 touchPoint = touch.position;
                    //touchPoint.z = Camera.main.transform.position.y;

                    Vector3 goal = new Vector3(player.transform.position.x, player.transform.position.y, 40);
                    //Vector3 velocity = touchPoint - player.transform.position;
                    Vector3 velocity = goal - player.transform.position;

                    ProjectileController pc = projectile.GetComponent<ProjectileController>();
                    pc.velocity = velocity.normalized * 100.0f;
                }
            }
        }
  //      if (Input.GetKey(KeyCode.Space)) {
		//	source.PlayOneShot (shootSound, 1f);
		//	GameObject projectile = GameObject.Instantiate<GameObject>(projectileTemplate);
		//	projectile.transform.parent = this.transform;
		//	projectile.transform.position = player.transform.position;

		//	//Vector3 touchPoint = touch.position;
		//	//touchPoint.z = Camera.main.transform.position.y;

		//	Vector3 goal = new Vector3(player.transform.position.x, player.transform.position.y, 0);
		//	//Vector3 velocity = touchPoint - player.transform.position;
		//	Vector3 velocity = goal - player.transform.position;

		//	ProjectileController pc = projectile.GetComponent<ProjectileController> ();
		//	pc.velocity = velocity.normalized * 50.0f;
		//}

	}
}
