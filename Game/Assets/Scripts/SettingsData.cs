﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SettingsData : MonoBehaviour {

	public float speed = 6.0f;
	public bool fixedSpeed;

	public Slider difficultySlider;
	public Toggle fixedSpeedChecker;

	void Start() {
		this.gameObject.SetActive (true);

//		SettingsData d = (SettingsData)FindObjectOfType (typeof(SettingsData));
//		while (d != null) {
//			Destroy (d);
//			d = (SettingsData)FindObjectOfType (typeof(SettingsData));
//		}

		difficultySlider = (Slider)FindObjectOfType (typeof(Slider));
		if (difficultySlider!=null)
			difficultySlider.onValueChanged.AddListener (delegate {setSpeed ();});
		fixedSpeedChecker = (Toggle)FindObjectOfType (typeof(Toggle));
		if (fixedSpeedChecker!=null)
			fixedSpeedChecker.onValueChanged.AddListener (delegate {setFixedSpeed ();});

		DontDestroyOnLoad(this);
	}

	void setFixedSpeed() {
		fixedSpeed = fixedSpeedChecker.isOn;
	}

	public void setSpeed() {
		speed = difficultySlider.value;
	}

}
