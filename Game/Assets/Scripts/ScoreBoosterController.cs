﻿using UnityEngine;
using System.Collections;

public class ScoreBoosterController : MonoBehaviour {

	public Camera player;
	public GameObject boosterTemplate;

	public Shader shader;
	public DirectionalLight directionalLight;

    private int frameCount;
	private float boosterSpeed;
	private GroundData groundData;
	private System.Random rand;

	private float minSpeed = 6.0f;
	private const float maxSpeed = 20.0f;
	private const float generateRate = 0.005f;
    private GameObject[] boosters;

    // Use this for initialization
    void Start () {
		frameCount = 0;
		groundData = new GroundData ();

		rand = new System.Random ();

    }
	
	// Update is called once per frame
	void Update () {
		frameCount++; 
		// Determine the speed of movement of obstacles according to the current difficulty. 
		SettingsData[] data = (SettingsData[]) FindObjectsOfType(typeof(SettingsData));
		SettingsData d = null;
		if (data.Length != 0) {
			d = data [data.Length-1];
		}

		Debug.Log (data);
		if (d != null) {
			// Define the speed of obstacles movement
			boosterSpeed = d.speed;
			minSpeed = d.speed;
			if (!d.fixedSpeed) {
				boosterSpeed = frameCount / 900.0f + minSpeed;
				if (boosterSpeed < minSpeed)
					boosterSpeed = minSpeed;
				if (boosterSpeed > maxSpeed)
					boosterSpeed = maxSpeed;
			}
		} else {
			boosterSpeed = minSpeed;
			boosterSpeed = frameCount / 900.0f + minSpeed;
			if (boosterSpeed < minSpeed)
				boosterSpeed = minSpeed;
			if (boosterSpeed > maxSpeed)
				boosterSpeed = maxSpeed;
		}

		//Debug.Log (frameCount);
		//Debug.Log (obstacleSpeed);

		double randNum = rand.NextDouble (); 
		//Debug.Log (rate); 
		if (randNum < generateRate) {
			generateNewBooster ();
		}
        renderBooster();
        moveBoosters ();

	}

	private void generateNewBooster() {
		GameObject booster = GameObject.Instantiate<GameObject>(boosterTemplate);
		booster.transform.parent = this.transform;

        int trackIndex = groundData.getNearestVerti(player.transform.position);
		int rightTrack = trackIndex + 1;
		if (rightTrack == 12)
			rightTrack = 0;
		int leftTrack = trackIndex - 1;
		if (leftTrack == -1)
			leftTrack = 11;

		Vector3 boosterPos;
		int randNum = rand.Next (1, 100);
		//Debug.Log (randNum);
		if (randNum < 33) {
			boosterPos = groundData.getObjectPos (rightTrack);
		} else if (randNum < 66) {
			boosterPos = groundData.getObjectPos (trackIndex);
		} else {
			boosterPos = groundData.getObjectPos (leftTrack);
		}


		//Debug.Log (obstaclePos);
		booster.transform.position = new Vector3(boosterPos.x, boosterPos.y, 0);
		booster.transform.eulerAngles = new Vector3(0, 0, boosterPos.z);

	}

	private void moveBoosters() {
		this.transform.Translate(Vector3.back * boosterSpeed/10);
	}

    private void renderBooster()
    {
        GameObject[] boosters = GameObject.FindGameObjectsWithTag("Booster");

        //GetComponentInChildren(MeshRenderer).material.shader = shader;
        if (boosters != null)
        {
            foreach (GameObject booster in boosters)
            {
                //Debug.Log(booster.transform.position);

                MeshRenderer renderer = booster.GetComponent<MeshRenderer>();
                renderer.material.shader = shader;
                renderer.material.SetColor("_PointLightColor", directionalLight.color);
                renderer.material.SetVector("_PointLightPosition", directionalLight.GetWorldPosition());
            }
        }
    }
}
