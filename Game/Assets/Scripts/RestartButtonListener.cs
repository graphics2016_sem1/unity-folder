﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class RestartButtonListener : MonoBehaviour {

	public Button btn;

	void Start() {
		Button b = btn.GetComponent<Button> (); 
		b.onClick.AddListener (RestartGame);

	}

	public void RestartGame() {
		//Debug.Log ("S");
		SceneManager.LoadScene ("GameScene");
	}
}
