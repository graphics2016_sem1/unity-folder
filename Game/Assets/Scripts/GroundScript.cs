﻿using UnityEngine;
using System.Collections;

public class GroundScript : MonoBehaviour
{

	public DirectionalLight directionalLight;
	public Shader shader;

	// Use this for initialization
	void Start()
	{
		// Add a MeshFilter component to this entity. This essentially comprises of a
		// mesh definition, which in this example is a collection of vertices, colours 
		// and triangles (groups of three vertices). 
		MeshFilter groundMesh = this.gameObject.AddComponent<MeshFilter>();
		groundMesh.mesh = this.CreateGroundMesh();

		// Add a MeshRenderer component. This component actually renders the mesh that
		// is defined by the MeshFilter component.
		MeshRenderer renderer = this.gameObject.AddComponent<MeshRenderer>();
		renderer.material.color = Color.grey;
		renderer.material.shader = shader;
	}

	// Method to create a cube mesh with coloured vertices
	Mesh CreateGroundMesh()
	{
		Mesh m = new Mesh();
		m.name = "Ground";

		// Define the vertices of the polygon that is near the player (i.e. camera)
		Vector2[] polygonVertices = new[] {

			new Vector2(0, 5),
			new Vector2(2.5f, 4.33f),
			new Vector2(4.33f, 2.5f), 

			new Vector2(5, 0),
			new Vector2(4.33f, -2.5f), 
			new Vector2(2.5f, -4.33f),

			new Vector2(0, -5),
			new Vector2(-2.5f, -4.33f),
			new Vector2(-4.33f, -2.5f), 

			new Vector2(-5, 0),
			new Vector2(-4.33f, 2.5f), 
			new Vector2(-2.5f, 4.33f)
		};

		Vector3[] vert_array = new Vector3[polygonVertices.Length * 6];

		int vert_i = 0;
		for (int i = 0; i < polygonVertices.Length * 6; i+=6) {
			if (vert_i == polygonVertices.Length-1) {

				// Outside tunnel
				vert_array [i] = new Vector3 (polygonVertices [0].x, polygonVertices [0].y, 40);
				vert_array [i + 1] = new Vector3 (polygonVertices [vert_i].x, polygonVertices [vert_i].y, -40);
				vert_array [i + 2] = new Vector3 (polygonVertices [vert_i].x, polygonVertices [vert_i].y, 40);
				vert_array [i + 3] = new Vector3 (polygonVertices [0].x, polygonVertices [0].y, 40);
				vert_array [i + 4] = new Vector3 (polygonVertices [0].x, polygonVertices [0].y, -40);
				vert_array [i + 5] = new Vector3 (polygonVertices [vert_i].x, polygonVertices [vert_i].y, -40);
			} else {

				// Outside tunnel
				vert_array [i] = new Vector3 (polygonVertices [vert_i + 1].x, polygonVertices [vert_i + 1].y, 40);
				vert_array [i + 1] = new Vector3 (polygonVertices [vert_i].x, polygonVertices [vert_i].y, -40);
				vert_array [i + 2] = new Vector3 (polygonVertices [vert_i].x, polygonVertices [vert_i].y, 40);
				vert_array [i + 3] = new Vector3 (polygonVertices [vert_i + 1].x, polygonVertices [vert_i + 1].y, 40);
				vert_array [i + 4] = new Vector3 (polygonVertices [vert_i + 1].x, polygonVertices [vert_i + 1].y, -40);
				vert_array [i + 5] = new Vector3 (polygonVertices [vert_i].x, polygonVertices [vert_i].y, -40);
			}
			vert_i++;
		}
		m.vertices = vert_array;


		// Define the vertex colours
		Color[] color_array = new Color[polygonVertices.Length * 6];
		for (int i = 0; i < polygonVertices.Length * 6; i+=6) {
			if (i % 12 == 0) {
				color_array [i] = new Color (0.8f, 0.8f, 0.8f, 1f); 
				color_array [i+1] = new Color (0.8f, 0.8f, 0.8f, 1f);
				color_array [i+2] = new Color (0.8f, 0.8f, 0.8f, 1f);
				color_array [i+3] = new Color (0.8f, 0.8f, 0.8f, 1f);
				color_array [i+4] = new Color (0.8f, 0.8f, 0.8f, 1f);
				color_array [i+5] = new Color (0.8f, 0.8f, 0.8f, 1f);
			} else {
				color_array [i] = Color.white;
				color_array [i+1] = Color.white;
				color_array [i+2] = Color.white;
				color_array [i+3] = Color.white;
				color_array [i+4] = Color.white;
				color_array [i+5] = Color.white;
			}
		}
		m.colors = color_array;


		Vector3[] vertexNormals = new Vector3[polygonVertices.Length * 6];
		for (int i = 0; i < polygonVertices.Length * 6; i++) {
			vertexNormals [i] = vert_array [i].normalized;
		}
		m.normals = vertexNormals;


		// Automatically define the triangles based on the number of vertices
		int[] triangles = new int[m.vertices.Length];
		for (int i = 0; i < m.vertices.Length; i++)
			triangles[i] = i;

		m.triangles = triangles;

		return m;
	}

	void Update() {
		// Get renderer component (in order to pass params to shader)
		MeshRenderer renderer = this.gameObject.GetComponent<MeshRenderer>();

		// Pass updated light positions to shader
		renderer.material.SetColor("_PointLightColor", this.directionalLight.color);
		renderer.material.SetVector("_PointLightPosition", this.directionalLight.GetWorldPosition());
	}
}
