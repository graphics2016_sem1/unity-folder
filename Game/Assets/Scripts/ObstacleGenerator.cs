﻿using UnityEngine;
using System.Collections;

public class ObstacleGenerator : MonoBehaviour {

	public Camera player;

	public GameObject obstacleTemplate;
	public GameObject fakeObstacleTemplate;

    public Shader fakeObShader;
    public Shader ObShader;
    public DirectionalLight pointLight;

    private int frameCount;
    private float obstacleSpeed;
	private GroundData groundData;
	private System.Random rand;

	//private int testnum;

    private const float minSpeed = 6.0f;
    private const float maxSpeed = 15.0f;
	private const float generateRate = 0.05f;

	// Use this for initialization
	void Start () {
        frameCount = 0;
		groundData = new GroundData ();

		rand = new System.Random ();
	}
	
	// Update is called once per frame
	void Update () {
		frameCount++; 
        // Determine the speed of movement of obstacles according to the current difficulty. 
		obstacleSpeed = frameCount / 900.0f + minSpeed;
        if (obstacleSpeed < minSpeed) obstacleSpeed = minSpeed;
        if (obstacleSpeed > maxSpeed) obstacleSpeed = maxSpeed;

		//Debug.Log (frameCount);
		//Debug.Log (obstacleSpeed);

		double randNum = rand.NextDouble (); 
		//Debug.Log (rate); 
		if (randNum < generateRate) {
			generateNewObstacle ();
		}

		moveObstacles ();
	}

	private void generateNewObstacle() {
		int randNum = rand.Next (0, 2);
		GameObject obstacle;
		if (randNum == 0) {
			obstacle = GameObject.Instantiate<GameObject> (obstacleTemplate);
		} else {
			obstacle = GameObject.Instantiate<GameObject> (fakeObstacleTemplate);
		}
		obstacle.transform.parent = this.transform;

		int trackIndex = groundData.getNearestVerti(player.transform.position);

		Vector3 obstaclePos;
		randNum = rand.Next (0, 15);
		if (randNum > 11) {
			obstaclePos = groundData.getObjectPos (trackIndex);
		} else {
			obstaclePos = groundData.getObjectPos (randNum);
		}
			
		obstacle.transform.position = new Vector3(obstaclePos.x, obstaclePos.y, 40);
		obstacle.transform.eulerAngles = new Vector3(0, 0, obstaclePos.z);

	}

	private void moveObstacles() {
		this.transform.Translate(Vector3.back * obstacleSpeed/10);
	}

	public float getCurrSpeed() {
		return this.obstacleSpeed;
	}

    private void shadeFakeObstacles()
    {
        GameObject[] fakeObstacles = GameObject.FindGameObjectsWithTag("FakeObstacle");
        
        if (fakeObstacles != null)
        {
            foreach (GameObject fakeObstacle in fakeObstacles)
            {
                //Debug.Log(booster.transform.position);

                MeshRenderer renderer = fakeObstacle.GetComponent<MeshRenderer>();
                renderer.material.shader = fakeObShader;
                renderer.material.SetColor("_PointLightColor", this.pointLight.color);
                renderer.material.SetVector("_PointLightPosition", this.pointLight.GetWorldPosition());
            }
        }
    }

    private void shadeObstacles()
    {

    }
}
