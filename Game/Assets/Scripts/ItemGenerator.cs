﻿using UnityEngine;
using System.Collections;

public class ItemGenerator : MonoBehaviour {

	public Camera player;
	public GameObject itemTemplate;

	private int frameCount;
	private float itemSpeed;
	private GroundData groundData;
	private System.Random rand;

	private float minSpeed = 6.0f;
	private const float maxSpeed = 20.0f;
	private const float generateRate = 0.002f;

	// Use this for initialization
	void Start () {
		frameCount = 0;
		groundData = new GroundData ();

		rand = new System.Random ();
	}
	
	// Update is called once per frame
	void Update () {
		frameCount++; 
		// Determine the speed of movement of obstacles according to the current difficulty. 
//		itemSpeed = frameCount / 900.0f + minSpeed;
//		if (itemSpeed < minSpeed) itemSpeed = minSpeed;
//		if (itemSpeed > maxSpeed) itemSpeed = maxSpeed;
//
		SettingsData[] data = (SettingsData[]) FindObjectsOfType(typeof(SettingsData));
		SettingsData d = null;
		if (data.Length != 0) {
			d = data [data.Length-1];
		}

		Debug.Log (data);
		if (d != null) {
			// Define the speed of obstacles movement
			itemSpeed = d.speed;
			minSpeed = d.speed;
			if (!d.fixedSpeed) {
				itemSpeed = frameCount / 900.0f + minSpeed;
				if (itemSpeed < minSpeed)
					itemSpeed = minSpeed;
				if (itemSpeed > maxSpeed)
					itemSpeed = maxSpeed;
			}
		} else {
			itemSpeed = minSpeed;
			itemSpeed = frameCount / 900.0f + minSpeed;
			if (itemSpeed < minSpeed)
				itemSpeed = minSpeed;
			if (itemSpeed > maxSpeed)
				itemSpeed = maxSpeed;
		}

		double randNum = rand.NextDouble (); 
		//Debug.Log (rate); 
		if (randNum < generateRate) {
			generateNewItem ();
		}

		moveItems ();
	}

	private void generateNewItem() {
		GameObject item = GameObject.Instantiate<GameObject>(itemTemplate);
		item.transform.parent = this.transform;
		//item.gameObject.tag = "Item";

		int trackIndex = groundData.getNearestVerti(player.transform.position);
		int rightTrack = trackIndex + 1;
		if (rightTrack == 12)
			rightTrack = 0;
		int leftTrack = trackIndex - 1;
		if (leftTrack == -1)
			leftTrack = 11;

		Vector3 itemPos;
		int randNum = rand.Next (1, 100);
		//Debug.Log (randNum);
		if (randNum < 33) {
			itemPos = groundData.getObjectPos (trackIndex);
		} else if (randNum < 66) {
			itemPos = groundData.getObjectPos (leftTrack);
		} else {
			itemPos = groundData.getObjectPos (rightTrack);
		}


		//Debug.Log (obstaclePos);
		item.transform.position = new Vector3(itemPos.x, itemPos.y, 40);
		item.transform.eulerAngles = new Vector3(0, 0, itemPos.z);

	}

	private void moveItems() {
		this.transform.Translate(Vector3.back * itemSpeed/10);
	}
}
