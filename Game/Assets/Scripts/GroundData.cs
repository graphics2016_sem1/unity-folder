﻿using UnityEngine;
using System.Collections;

public class GroundData {

	private Vector2[] polygonVertices;

	private Vector3[] obstaclePosition;

	public GroundData() {
		polygonVertices = new[] {

			new Vector2 (0, 5),
			new Vector2 (2.5f, 4.33f),
			new Vector2 (4.33f, 2.5f), 

			new Vector2 (5, 0),
			new Vector2 (4.33f, -2.5f), 
			new Vector2 (2.5f, -4.33f),

			new Vector2 (0, -5),
			new Vector2 (-2.5f, -4.33f),
			new Vector2 (-4.33f, -2.5f), 

			new Vector2 (-5, 0),
			new Vector2 (-4.33f, 2.5f), 
			new Vector2 (-2.5f, 4.33f)
		};

		obstaclePosition = new[] {

			new Vector3 (0.0f, 6.12463f, 0.0f), 
			new Vector3 (3.0623f, 5.3041f, 330.0f),
			new Vector3 (5.3041f, 3.0623f, 300.0f), 
			new Vector3 (6.12463f, 0.0f, 270.0f), 
			new Vector3 (5.3041f, -3.0623f, 240.0f),
			new Vector3 (3.0623f, -5.3041f, 210.0f), 
			new Vector3 (0.0f, -6.12462f, 180.0f), 
			new Vector3 (-3.0623f, -5.3041f, 150.0f),
			new Vector3 (-5.3041f, -3.0623f, 120.0f),
			new Vector3 (-6.12403f, 0.0f, 90.0f),
			new Vector3 (-5.3041f, 3.0623f, 60.0f),
			new Vector3 (-3.0623f, 5.3041f, 30.0f)

		};
	}

	public int getNearestVerti(Vector3 pos) {
		if (pos.y > 4.83) {
			return 0;
		} else if (pos.y < -4.83) {
			return 6;
		}
		if (pos.x > 4.83) {
			return 3;
		} else if (pos.x > 3.536) {
			if (pos.y > 0) {
				return 2;
			} else {
				return 4;
			}
		} else if (pos.x > 1.295) {
			if (pos.y > 0) {
				return 1;
			} else {
				return 5;
			}
		} else if (pos.x > -3.536) {
			if (pos.y > 0) {
				return 11;
			} else {
				return 7;
			}
		} else if (pos.x > -4.83) {
			if (pos.y > 0) {
				return 10;
			} else {
				return 8;
			}
		} else {
			return 9;
		}
	}

	public Vector2 getVert(int i) {
		//Debug.Log (i);
		//Debug.Log (polygonVertices.Length);
		return (polygonVertices [i]);
	}

	public Vector3 getObjectPos(int i) {
		return (obstaclePosition [i]);
	}
}
