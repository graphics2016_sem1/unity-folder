﻿Shader "Unlit/ScoreBoosterShader"
{
	Properties{
		_MainTex("Main Texture", 2D) = "white" {}
		_BumpMap("Normal Map", 2D) = "bump" {}
		_RimColor("Rim Color", Color) = (1,1,1,1)
		_RimPower("Rim Power", Range(0.0, 8.0)) = 5.0
		_Alpha("Alpha", Range(0.5, 0.9)) = 0.7
	}
		SubShader{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }


		CGPROGRAM
		#pragma surface surf Lambert alpha

		struct Input {
		float4 color : COLOR;
		float2 uv_MainTex;
		float2 uv_BumpMap;
		float3 viewDir;
		};

		sampler2D _MainTex;
		sampler2D _BumpMap;
		float4 _RimColor;
		float _RimPower;
		float _Alpha;

		void surf(Input IN, inout SurfaceOutput o) {
			o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
			o.Alpha = _Alpha;
			half rim = 1.0 - saturate(dot(normalize(IN.viewDir), o.Normal));

			o.Emission = _RimColor.rgb * pow(rim, _RimPower);
		}
		ENDCG
	}
	FallBack "Diffuse"
}
