Project Name: Chasing the Light





"Chasing the Light" is a 3D endless running game. The player races down a 12-sided cylinder in order to chase the floating illuminant. At the same time, the player needs to avoid running into the approaching obstacles by tilting the screen to move left and right. Items and score boosters can be collected on the way. The item allows the player to destroy some of the obstacles by touching the screen. 






Scenes



Menu scene

The welcome scene of the game, with three buttons namely "Start" (start the game), "How to play" (the guidance of the game), "Settings" (setting of personal preferences of the game). 



Instruction scene

Contains the instruction of the game. 



Settings scene

Player can set the initial speed, and can decide whether to increase the speed during the game or not. 



Game scene

The main game scene. 







Game Control


Accelerometer (main control of the game): 

Sideway movement is performed by tilting the screen. 



Touch input:

Basic options of the game;

Shoot projectiles when an item is activated.







Modelling, Graphics, and Camera Motion



Skybox

The skybox is downloaded from Asset Store and set as lighting in all the scenes. 



Stars

The stars is a particle system at the end of the cylinder and emit particles with a fixed rate.



Light source

The light source of the game is a directional light with a fixed position local to the player. 



Player (Camera Motion)

The game is carried out in the first person perspective, hence the main camera is acting as the player in the game. By accelerometer control, the camera rotate around the central axis of the 12-sided cylinder with a certain radius. 



Path

The path of the player is modelled by a 12-sided cylinder. Phong shader is applied to the path in order for the player to see the ground clearly. 



Obstacles

Obstacles in the game are modelled by cubes. Two different textures are applied to the cubes indicating which kind of cubes can be destroyed and which kind cannot. Bump mapping is applied to the normal cubes which can be break when the player shoot it. Fresnel Shading is using on the shootable obstacles to make it more shiny. 



Chasing Light

This light is combined with 5 particle system components, which has smoking, dust and twinkling effect. The Gameobject rotates around the cylinder with a fixed distance from the player.



Items

Items are made of 4 particle systems with glowing effect to show its special function which is to enable the player to break the obstacles on the path for a certain period when the player touch the screen. 

A rim shader is applied to all the items to make it half transparent with a white outer rim.

Score boosters

A Capsule model is used to represent the score booster. It has the special function to increase player’s score by 100. It is textured with emitted yellow colour to attract the players’ attentions.



Projectiles

The projectiles shot by the player are just plain spheres with no special effects applied. 






Credits:

Background music and sound effects: www.playonloop.com
Skybox: https://www.assetstore.unity3d.com/en/#!/content/25117
Fresnel Shading: https://github.com/VoxelBoy/Resources-for-Writing-Shaders-in-Unity


Gameplay video:
https://youtu.be/lkLGY-idajs
